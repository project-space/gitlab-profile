# SPHN NDS and DEM Projects

Welcome to the NDS and DEM Projects Group! This `GitLab group` provides a dedicated space for each SPHN-funded project to upload all necessary documents required for a Data Transfer Request (DTR) through a predefined repository structure (see below) or other `GitLab projects` at the discretion of the project.

Each NDS and DEM project has its own sub-group where they can create, by default private, `GitLab projects` as needed.

## Support

Your designated RDF Support person is you primary contact for support in preparing your project space.

In case of need, you can also contact the FAIR Data Team: fair-data-team@sib.swiss


## Folder structure

To facilite the easy retrieval of all necessary documents in the context of a DTR, projects are required to adhere to a rigourous folder structure (this doens't involve the project private `GitLab project`)

The project's GitLab repository will serve as the sole access point for the data provider to retrieve all required documents in accordance with the SPHN Semantic Interoperability Framework compliance and the process for Data Transfer Requests document.

You can access the latest version of the document here: https://sphn.ch/document/dtr-process-description/


**Project RDF Schema: Folder Structure**

Group: `Project Name`<br>
Repository: `rdf-schema`<br>
- Folder: `version-1`
    - Subfolder: `dataset`
    - Subfolder: `schema`
    - Subfolder: `shacl`
    - Subfolder: `sparql`
    - Subfolder: `doc`
- Folder: `version-2`<br>
(...)

Note: Only two **official** versions of a project-specific RDF schema can be delivered.


**Data Transfer Request: Folder Structure**

Group: `Project Name`<br>
Repository: `data-transfer`<br>
- Folder: `data-transfer-1`
    - ReadMe<br>
    Data transfer specification
        - Status (in preparation/active/on hold/ stopped)
        - Frequency of data transfers:
        - Timeline:
        - Providers:
        - Cohort specification: link to document on Git
        - De-identification rules: link to the document on Git
        - RDF Schema: link to schema on Git e.g., SPHN 2023.2 or LUCID 2023.1
        - Reused SPHN concepts: link to csv on Git
        - Requested project concepts: link to csv on Git
        - RDF data format: (.ttl/.trig/.nq)
        - Comments:
    - Subfolder: `cohort-specification`
    - Subfolder: `reused-sphn-concepts`
    - Subfolder: `requested-project-concepts`
- Folder: `data-transfer-2`<br>
(...)

**Other important documents: Folder structure**

In addition to the RDF-Schema and DTR specification. The project need to provide a stable link to the De-Idenfication rules applied to the data, a link to the ethic documents and eventual external terminologies in use within the project.

Group: `Project Name`<br>
Repository: `external-terminologies`<br>
Repository: `de-identification-rules`<br>
Repository: `ethics-documents`<br>

## How to request RDF data according to concepts

In the context of a data transfer, to receive RDF data according to the SPHN or project-specific schema, the sub-folders 'reused-sphn-concepts' and 'requested-project-concepts' must be populated with the correct information.

The content of these folders needs to provide enough information about which concepts of the schema need to be delivered by the data provider and, if necessary, according to which special requirements.

It is recommended to populate the 'reused-sphn-concepts' and 'requested-project-concepts' folders with a .csv or .xlsx file listing the desired concepts for a specific data transfer as a table. This table should include the following columns: 'Concept', 'Standards', and 'Comments'.

Note: The choice of the format and structure is up to the project; the above is a recommendation.

**Important**: Only concept names related to top-level concepts (directly connected to a patient) should be represented in the list. Any nested concepts will be delivered if available.

Example of annexed file structure:

| Concept | Standards | Comment |
|----------|----------|----------|
| Concept name 1    |  Standard A |    |
| Concept name 2    |    | Please provide these values for Concept name 2 -> Nested attribute   (List of values)|
| Concept name 3    | Standard B  |    |

In the example, specific requirement aimed at nested attribute (or concept) of 'concept 2' are indicated using arrows (->) to show the relationship within the concept.

More complex requirements or instructions can be delivered in separated file which should be also included in the specifications.




